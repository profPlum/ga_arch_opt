# NOTE: scripts can use `if (getOption('main', default=TRUE)) { ... }`
# to wrap main code (like in python)
source_local <- function(fn) {
  options(main=FALSE)
  source_dir <- dirname(parent.frame(3)$ofile)
  source(paste0(source_dir, '/', fn))
  options(main=TRUE)
}
source_local('Visualize_GA_data.R')

data_master_dir <- '/Users/dfire/Desktop/all-GA-batch-sets/test-GA-batch-trials-set2/'
setwd(data_master_dir)
data_sub_dirs <- c('standard', 'erratic', 'mu+lambda', 'mu+lambda/elitism', 'mu+lambda/elitism/erratic') # sub dirs
data_sub_dirs <- c(data_sub_dirs, 'mu+lambda/erratic', 'monte-carlo')
data_dirs <- data_sub_dirs

# load GA data
collect_GA_trials_binding <- partial(collect_GA_trials, num_trials=5,
                                     num_gens=25, of_pop=F)
data_sets <- map(data_dirs, collect_GA_trials_binding)

# make experiment specific plots (this takes a while, can be worth skipping)
# cat('making experiment specific plots, this can take a while...\n')
# walk2(data_sets, data_dirs, make_plots_for_data)

################### GA CONFIG COMPARISON ###################

# NOTE: possible experiment~
# * break trial column into multiple using tidyr
#     > e.g. elitism, erratic, mu+lambda (bools)
# * create model matrix using multiple cols (indicator vars)
# * have model predict using these variables to determine best

fit_rate_of_convergence <- function(config_as_trial_dataset, quite=F) {
  # general convergence formula is: Fit(x) = Cx^r
  # fit lm's to log(fit) ~ gen (log takes it out of exponent)
  conv_rate_fits <- config_as_trial_dataset %>%
    summarize_GA_data('best', quos(trial, gen, Fitness.weighted)) %>%
    group_by(trial) %>%
    do(mod=lm(log(Fitness.weighted) ~ gen, data=.)) %>%
    ungroup()

  # record lm coeficient as R (first 'coeficient' is intercept)
  conv_rate_fits <- conv_rate_fits %>% rowwise %>%
    mutate(R=coef(mod)[2]) %>%
    ungroup %>% arrange(desc(R))

  # print model summaries
  if (!quite) {
    summaries <- conv_rate_fits$mod %>%
      map(summary)
    names(summaries) <- conv_rate_fits$trial
    print(summaries)

    # view convergence rates
    View(conv_rate_fits %>% select(-mod))
  }
  return(invisible(conv_rate_fits))
}

configs <- as.factor(data_sub_dirs)
master_dataset <- data_sets %>%
  map2(configs, ~mutate(.x, ga_config=.y)) %>%
  reduce(rbind)

#TODO: make spread plots where spread is computed individually by trial?
config_as_trial_dataset <- master_dataset %>%
  select(-trial) %>%
  rename(trial=ga_config)

config_as_trial_dataset %>%
  make_plots_for_data(subtitle='config comparison', baseline=construct_baseline(),
                      best_only=T, save=T)

fit_rate_of_convergence(config_as_trial_dataset)
