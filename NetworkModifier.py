"""
NetworkModifier and Supplemental GA classes:
These take care of actually modifying networks for testing...

The reason that these classes are bundled together is that NetworkModifier owns
the relevant chromosomes since it was simpler to let deap still use lists...

TERMINOLOGY:
    * 'hyper-param' (sometimes ambiguously referred to as variable or arg) is a parameter to the
      network who's value isn't normally learned and thus is expected to be provided by the programmer
      however the whole point in this script is be able to 'learn' those too

    * 'setting' means a config variable to the network wrapper (such as epochs or learning rate)

    * 'mutable' means that something is or contains valid variables to be 'mutated' by the genetic algorithm,
      this includes all *numeric* 'settings' (cfg variables) or arguments to 'mutable layers'
      WITH THE EXCEPTION OF numeric tuple arguments as these usually denote important shapes
      which shouldn't change (like input and output shapes of the network)
"""

import inspect
import re
import types
from copy import deepcopy, copy

import keras
import numpy as np
import yaml

from dep.utils import NUM_T, get_config_path, str_to_num, is_basic_num


# verified to work 6/28/18
# ^ minor changes since with unbounding
class MutableArg:
    """ represents a mutable hyper-param """

    # TODO: point this towards another class when NetMod gets refactored
    @classmethod
    def get_mapped_range_spread(cls):
        return NetworkModifier.mapped_range_spread

    @classmethod
    def get_bound_mutation_to_mapped_range(cls):
        return NetworkModifier.bound_mutation_to_mapped_range

    @classmethod
    def get_known_range_restrictions(cls):
        return NetworkModifier.known_range_restrictions

    def is_in_valid_range(self, raw_val):
        return self._range_r.valid_range[0] <= raw_val <= self._range_r.valid_range[1]

    # NOTE: accommodate not very relevant anymore...
    def compute_mapped_range(self, raw_val, accommodate_only=False):
        """
        (re)computes mapped range from a valid raw value
        :param raw_val: the raw value that will be used, should be 'the default' if accommodate_only==False
        :param accommodate_only: if True will not recompute the mapped range but just adjust it to include the new value
        :return: nothing
        """
        val_type = type(raw_val)

        # min and max of the min and max of a mapped range
        try:
            self._range_r: RangeR = self.get_known_range_restrictions()[self._name]
        except KeyError:
            if val_type is int:
                self._range_r = self.get_known_range_restrictions()['int_default']
            else:
                assert val_type is float
                self._range_r = self.get_known_range_restrictions()['float_default']

        # check that raw value is inside 'valid_range' (note: this double comparison does work)
        if not (self.is_in_valid_range(raw_val)):
            val_prefix = 'accommodated' if accommodate_only else 'default'
            raise ValueError("{} value: {}, of {}, is outside of valid range: [{}, {}]"
                             .format(val_prefix, raw_val, self._name, *self._range_r.valid_range))

        if accommodate_only:
            assert val_type is type(self.mapped_range[0])
            min_ = min(self.mapped_range[0], raw_val)
            max_ = max(self.mapped_range[1], raw_val)
            mapped_range = [min_, max_]
        else:

            mapped_range = [val_type(raw_val * (1 - self.get_mapped_range_spread())),
                            val_type(raw_val * (1 + self.get_mapped_range_spread()))]

            # make it at least span min span
            mapped_range[0] = val_type(min(mapped_range[0], raw_val - self._range_r.min_span/2))
            mapped_range[1] = val_type(max(mapped_range[1], raw_val + self._range_r.min_span/2))

            # crop to lower valid boundary
            mapped_range[0] = val_type(max(mapped_range[0], self._range_r.valid_range[0]))
            # crop to upper valid boundary
            mapped_range[1] = val_type(min(mapped_range[1], self._range_r.valid_range[1]))

        self._mapped_range = tuple(mapped_range)

    def __init__(self, def_val: NUM_T, name: str = 'unknown'):
        """ def_val & name are required arguments, they just have defaults for the yaml loader """
        self._name = name
        self._val = def_val

        # initialized by func below
        self._mapped_range: tuple = None
        self.compute_mapped_range(def_val)

    def load_from(self, other):
        if self.name != other.name or not (self.is_in_valid_range(other.mapped_ranged[0]) and
                                           self.is_in_valid_range(other.mapped_ranged[1])):
            raise RuntimeError('You\'re attempting to load old an invalid chromosome')
        else:
            self._mapped_range = other.mapped_ranged

    @property
    def raw_val(self):
        return self._val

    @raw_val.setter
    def raw_val(self, new_val):
        if new_val < self.mapped_range[0] or new_val > self.mapped_range[1]:
            err_msg = '{} assigned to MutableArg "{}" & outside of mapped range: {}'
            err_msg = err_msg.format(new_val, self.name, self.mapped_range)
            raise ValueError(err_msg)

        self._val = new_val

    @property
    def name(self):
        return self._name

    @property
    def mapped_range(self):
        return self._mapped_range

    # helper functions that map raw vals to norm & visa versa
    def _map_to_norm(self, raw_val):
        return (raw_val - self.mapped_range[0]) / float(self.mapped_range[1] - self.mapped_range[0])

    def _map_to_raw(self, norm_float):
        val_type = type(self.raw_val)
        return val_type(self.mapped_range[0] + norm_float * float(self.mapped_range[1] - self.mapped_range[0]))

    @property
    def norm_float(self):
        """ the normalized raw_val of this mutable arg, using an affine transformation """
        return self._map_to_norm(self.raw_val)

    @norm_float.setter
    def norm_float(self, flt_val):
        """
        assigns to the norm_float affine mapping of raw_val (which appropriately affects raw_val),
        if flt_val is outside of corresponding valid range then it is clipped to become valid
        """

        # force float into valid/mapped range
        if not self.get_bound_mutation_to_mapped_range():
            # NOTE: it is can be allowed to wander outside of [0,1]
            # because the point in the mutation rate is to allow
            # exploration into any (valid) parameter space
            max_valid = self._map_to_norm(self._range_r.valid_range[1])
            min_valid = self._map_to_norm(self._range_r.valid_range[0])
        else:
            max_valid = 1.0
            min_valid = 0.0

        flt_val = min(flt_val, max_valid)
        flt_val = max(flt_val, min_valid)

        self._val = self._map_to_raw(flt_val)


class Chromosome(object):

    def __init__(self, mutable_args):
        self._mutable_args: list[MutableArg] = mutable_args

    def __len__(self):
        return len(self._mutable_args)

    def __str__(self):
        info_str = self.__class__.__name__ + '('

        for gene_name, gene in zip(self.gene_names, self.raw_genes):
            info_str += '{} = {}, '.format(gene_name, gene)

        # remove ', ' then add closing parens
        return info_str[:-2] + ')'

    # not really relevant anymore now that we aren't bounding inside mapped range...
    # actually nvm it is still sort'f if it runs up against the valid range boundary
    def make_compare_func(self):
        """
        makes a function that can compare two float-list individuals,
        this is used by hall of fame logger to decide when to replace hall-of-famers
        :param chromosome: a chromosome with the appropriate valid_ranges
        :return: similar_func
        """
        x_chrom = deepcopy(self)
        y_chrom = deepcopy(self)

        def compare(x, y):
            x_chrom.norm_floats = x
            y_chrom.norm_floats = y

            for x_gene, y_gene in zip(x_chrom.raw_genes, y_chrom.raw_genes):
                if x_gene != y_gene:
                    return False
            return True

        return compare

    # not super relevant now that mapped ranges aren't hard boundaries
    # todo: consider deleting
    def recompute_mutable_arg_mapped_ranges(self, best_individual: list = None, other_individuals=[]):
        """
        recomputes the mapped ranges of our mutable args
        by supplying them new 'default values' & accommodating others.
        mostly used for loading previous checkpoints for seeding or resuming
        :param best_individual: list of raw genes to be used as 'default values' for
                              computing mapped ranges, if None then current stored
                              values are used
        :param other_individuals: optional list of individuals (raw gene lists)
                                to also accomodate in the new mapped ranges
        """

        if best_individual is None:
            best_individual = self.raw_genes

        best_individual = list(best_individual)

        if len(best_individual) != len(self):
            raise RuntimeError('invalid checkpoint yaml loaded! please delete it or turn off --seed/--resume flag')

        for def_raw_gene, mut_arg in zip(best_individual, self._mutable_args):
            mut_arg.compute_mapped_range(def_raw_gene)

        for other_individual in other_individuals:
            for raw_gene, mut_arg in zip(other_individual, self._mutable_args):
                mut_arg.compute_mapped_range(raw_gene, accommodate_only=True)

    def load_from(self, chromosome):
        """for resuming only"""

        for marg1, marg2 in zip(self.mutable_args, chromosome.mutable_args):
            marg1.load_from(marg2)

    @property
    def norm_floats(self):
        return (arg.norm_float for arg in self._mutable_args)

    @norm_floats.setter
    def norm_floats(self, floats):
        for arg, flt in zip(self._mutable_args, floats):
            arg.norm_float = flt

    @property
    def raw_genes(self):  # usually ints
        return (arg.raw_val for arg in self._mutable_args)

    @raw_genes.setter
    def raw_genes(self, raw_vals):
        for arg, raw_v in zip(self._mutable_args, raw_vals):
            arg.raw_val = raw_v

    @property
    def gene_names(self):
        return (arg.name for arg in self._mutable_args)

    @property
    def unique_gene_names(self):
        names_used = set()
        for arg in self._mutable_args:
            name_id = 0
            name = arg.name
            while name in names_used:
                name_id += 1
                name = arg.name + str(name_id)
            names_used.add(name)
            yield name

    @property
    def mutable_args(self):
        return copy(self._mutable_args)


class RangeRestrictions(yaml.YAMLObject):
    """
    POD of known restrictions for mapped ranges of certain MutableArgs,
    referred to categorically by arg name
    """
    yaml_tag = u'!RangeRestrictions'

    def __init__(self, valid_range: tuple, min_span: NUM_T):
        """
        :param valid range: is a tuple or list like (min_valid, max_valid) that bound
        a MutableArg's mapped range (which is computed based on its default value)
        :param min_span: minimum span of a MutableArg's mapped_range,
        the valid_range requirement take priority over this one if they contradict
        """
        self.valid_range = valid_range
        self.min_span = min_span

    def __repr__(self):
        r_str = self.__class__.__name__
        r_str += '(valid_range={}, min_span={})'.format(self.valid_range, self.min_span)
        return r_str


RangeR = RangeRestrictions


# NOTE: this will not modify tuple arguments because these are often
# shapes (i.e. output shape) and changing shapes can break things
class NetworkModifier(object):
    """ used to extract and modify mutable args from NN settings and build src strings """

    ######################## CONFIGURABLE VALUES ########################

    # update these as needed, offline, they are assumed more or less constant at runtime
    raw_build_func_names = [
        '_build_clean_model']  # name of function(s) that build the network layers (NOT ITS WRAPPER!)
    mutable_layer_names = ('Conv1D', 'Conv2D', 'Dense', 'Dropout')  # , 'MaxPooling1D', 'MaxPooling2D')
    mutable_settings_names = ['adam_beta1', 'adam_beta2', 'target_data_amp',
                              'batch_size']  # 'learning_rate', 'patience'] #, 'epochs'] #
    # mutable_settings_names.extend(['_target_norm.mean', '_target_norm.mad', 'cutoff_val']) # for rel_loss (deprecated)

    # spread portion from mean of the mapped
    # range of a mutable arg before valid range is applied
    mapped_range_spread = 0.65

    # it seems that bounding it to mapped range by default is simpler and thus more desirable
    bound_mutation_to_mapped_range = True
    # if False then it is bound only to valid range, allowing it to explore further

    # These are the manually specified range restrictions of mutable_args with a given names
    known_range_restrictions = {'int_default': RangeR((1, 600), 5),  # REQUIRED, don't delete
                                'float_default': RangeR((0.0, 1.0), 0.001),  # REQUIRED, don't delete
                                'Dropout:rate': RangeR((0.0, 0.5), 0.2),
                                'Setting:learning_rate': RangeR((0.00001, 0.001), 0.0002),
                                'Setting:batch_size': RangeR((32, 64), 10),
                                'Setting:_target_norm.mean': RangeR((0.0, 40.0), 5.0),
                                'Setting:_target_norm.mad': RangeR((0.2, 15.0), 1.0),
                                'Setting:adam_beta1': RangeR((0.8, 0.999), 0),
                                'Setting:adam_beta2': RangeR((0.95, 0.99999999999), 0),
                                'Setting:target_data_amp': RangeR((0.4, 1500.0), 100)}

    # IMPORTANT: remove *whole* code elements, like layers, lines, or arguments,
    # any partial banned code pattern could mess with other patterns used later.
    # ALSO NOTE: comments will be removed from source before banned patterns are removed.
    # this is to make banned_patterns less complicated (like below)
    banned_patterns = [r"\n.*keras\.layers\.Dense\(.*\n*.*\).*\n+\s*return"]
    # ban last layer, can't let it get modified because it's output shape is important

    ####################################################################

    cfg_name = None  # name of config file loaded

    @classmethod
    def save_cfg(cls, cfg_path):

        cfg_dict = {'raw_build_func_names': cls.raw_build_func_names, 'mutable_layer_names': cls.mutable_layer_names,
                    'mutable_settings_names': cls.mutable_settings_names, 'banned_patterns': cls.banned_patterns,
                    'known_range_restrictions': cls.known_range_restrictions, 'mapped_range_spread': cls.mapped_range_spread,
                    'bound_mutation_to_mapped_range': cls.bound_mutation_to_mapped_range}

        with open(cfg_path, 'w') as f:
            yaml.dump(cfg_dict, f)

    @classmethod
    def load_cfg(cls, cfg_name):
        """
        load a network modifier config,
        checks local dir & configs directory
        :param cfg_name: name of net_mod config
        :return: nothing
        """

        with open(get_config_path(cfg_name), 'r') as f:
            cfg_dict = yaml.load(f)

        for var_name, val in cfg_dict.items():
            print('setting cls.{} = {}'.format(var_name, val))
            exec('cls.{} = val'.format(var_name))

        cls.cfg_name = cfg_name

    def __init__(self, net_wrapper):
        self._build_src_fmt_str = ''

        self._net_wrapper = net_wrapper

        # update source then get mutable args
        self._build_src_str = ''
        for raw_build_func_name in self.raw_build_func_names:
            source = inspect.getsource(getattr(self.net_wrapper, raw_build_func_name))
            source = self._change_source_indent(source, 0)  # zero indent for compiling

            # add a newline to be safe
            self._build_src_str += '\n' + source
        src_mutable_args = self._extract_mutable_args_from_source()
        mutable_settings = self._extract_mutable_args_from_settings()

        # update mutable args in chromosomes
        self._chromosome = Chromosome(src_mutable_args + mutable_settings)
        self._mutable_src_args_chrom = Chromosome(src_mutable_args)
        self._mutable_settings_chrom = Chromosome(mutable_settings)

        self._default_chromosome = deepcopy(self.chromosome)

    def __del__(self):
        # if this can cause odd exceptions on program exit, but they can be safely
        # ignored it is due to python deallocating modules before this for some reason
        try:
            self.reset_network()
        except AttributeError:
            pass

    @staticmethod
    def _change_source_indent(src, indent):
        tab_match = re.search('^\t+', src, flags=re.MULTILINE)

        if tab_match:
            # convert source to use spaces
            src = re.sub('\t', ' ' * 4, src)

        # matches to start of str
        base_indent_match = re.match(' *', src)
        base_indent = len(base_indent_match.group(0)) // 4

        # manually set base indent
        src = re.sub('^ {%d}' % (base_indent * 4), ' ' * (indent * 4), src, flags=re.MULTILINE)

        return src

    @property
    def net_wrapper(self):
        """if you want to change this you may aswell make a new NetModifier"""
        return self._net_wrapper

    @property
    def net_build_src(self):
        """source code of the network's build function"""
        return self._build_src_str

    @property
    def build_src_fmt_str(self):
        """unformated source of the network's build function"""
        return self._build_src_fmt_str

    @property
    def default_chromosome(self) -> Chromosome:
        return self._default_chromosome

    @property
    def chromosome(self) -> Chromosome:
        """get the chromosome of the solution currently used by the network"""
        return self._chromosome

    @property
    def mutable_src_args_chrom(self):
        """ gives the part of the basic chromosome that contains the mutable args """
        return self._mutable_src_args_chrom

    @property
    def mutable_settings_chrom(self):
        """ gives the part of the basic chromosome that contains the mutable settings """
        return self._mutable_settings_chrom

    def apply_chromosome(self, chromosome=None):
        """applies chromosome to the network"""
        if chromosome is not None:
            self.chromosome.raw_genes = chromosome.raw_genes

        ############################ apply settings: ##############################

        for mutable_setting in self.mutable_settings_chrom.mutable_args:
            # remove "Setting:" prefix
            real_setting_name = mutable_setting.name[len('Setting:'):]
            exec('self.net_wrapper.{} = mutable_setting.raw_val'.format(real_setting_name))

        ############################ apply source: ################################

        src = self._build_src_fmt_str.format(*self.mutable_src_args_chrom.raw_genes)

        # self isn't necessary for a simple definition
        local_ns = {}
        global_ns = {'keras': keras, 'K': keras.backend, 'np': np}

        # build new raw build function(s)
        exec(src, global_ns, local_ns)
        for raw_build_func_name in self.raw_build_func_names:
            raw_build_func = local_ns[raw_build_func_name]
            raw_build_func = types.MethodType(raw_build_func, self.net_wrapper)
            # make into method to bind 'self' parameter

            # update raw build function
            setattr(self.net_wrapper, raw_build_func_name, raw_build_func)

        self._build_src_str = src

    def reset_network(self):
        """rolls back all network modifications and their corresponding chromosomes"""
        self.apply_chromosome(self.default_chromosome)
        self.chromosome.load_from(self.default_chromosome)
        # order here doesn't matter

    @staticmethod
    def _extract_pattern_occurrences(pattern, str_, repl_token='{}'):
        """
        'extract' means to find all instances of a desired pattern in some string
        and extract them into a list using regex then replace those instances in the original string with
        a formatting operator (such as {} or %s), this way they can easily be reinserted after modification
        (i.e. with src.format(*modified_elements))

        this is exactly what this function does
        :param pattern: pattern of element to be extracted
        :param str_: string to extract from
        :param repl_token: usually a formatting operator to replace the pattern with
        :return: pattern_occurrences,  new_string
        """
        assert (repl_token not in str_)

        occurrences = re.findall(pattern, str_)
        str_ = re.sub(pattern, repl_token, str_)
        return occurrences, str_

    @classmethod
    def _extract_banned_patterns(cls, build_str):
        """
        extracts code banned in banned_patterns,
        from self._build_src_str
        :return: banned_code dictionary
        """

        # remove all comments right off the bat because it makes
        # certain banned patterns more complicated
        comment_pat = re.compile(r'^[ \t]*#.*$', flags=re.MULTILINE)
        build_str = re.sub(comment_pat, repl='', string=build_str)

        print('src before banned_patterns removed:')
        print(build_str)

        banned_code = {}

        for i, banned_pattern in enumerate(cls.banned_patterns):
            token = '#BANNED%i#' % i
            results = cls._extract_pattern_occurrences(pattern=banned_pattern, str_=build_str,
                                                       repl_token=token)
            banned_code[token], build_str = results

        print('src after banned_patterns removed:')
        print(build_str)

        return banned_code, build_str

    @classmethod
    def _replace_banned_code(self, banned_code, build_str):
        """
            replaces banned code in self._build_src_str
            that was extracted by _extract_banned_patterns()
            :param banned_code: the banned_code
                dictionary returned from
                _extract_banned_patterns()
        """

        # temporarily remove all '{}' from source
        build_str = re.sub(r'{}', repl='#FMT#', string=build_str)

        for token, code_strs in banned_code.items():
            # replaced #BANNED# token with format token then format...
            build_str = re.sub(token, repl=r'{}', string=build_str)
            build_str = build_str.format(*code_strs)

        # replace all '{}' in source
        build_str = re.sub('#FMT#', repl=r'{}', string=build_str)

        print('src with banned_patterns replaced:')
        print(build_str)

        return build_str

    @classmethod
    def _extract_mutable_args_from_layer(cls, layer_str):
        """
        extracts mutable args in numeric form from a layer's source,
        see _extract_pattern_occurrences() for explanation of 'extract'
        :param layer_str: a layer's source code
        :return: mutable_args, layer_source_modified
        """

        # saves layer name for later
        layer_name_pat = re.compile(r'\w+?(?= *\()')
        layer_name = layer_name_pat.match(layer_str).group()

        # gets arg list from layer constructor with no surrounding parens
        arg_list_pattern = re.compile(r'(?<=\().*(?=\))', flags=re.DOTALL)
        arg_list_str = arg_list_pattern.search(layer_str).group()
        layer_str = arg_list_pattern.sub('{}', layer_str)

        # we ignore tuple/list args
        tuple_arg_pattern = re.compile(r'(?:\(.*?\)|\[.*?\])', flags=re.DOTALL)
        tuple_args, mutable_arg_list_str = cls._extract_pattern_occurrences(tuple_arg_pattern, arg_list_str,
                                                                            repl_token='{}')

        # just a pattern for single (positive) numbers
        numeric_val_pattern = r'((?<![A-Za-z_])\d(?:\d|\.)*)'

        # pattern for potentially keyworded numeric args, that captures arg name and values
        mutable_arg_pattern = re.compile(r'(?:(\w+)\s*=\s*)?{}\s*,?'.format(numeric_val_pattern))

        # finally extract the mutible args
        # use %s so that we don't mix with the {} from tuple args
        # this was verified to work experimentally on 6/18/18
        mutable_args = mutable_arg_pattern.findall(mutable_arg_list_str)
        mutable_arg_list_str = re.sub(numeric_val_pattern, '%s', mutable_arg_list_str)

        # cast to numeric types
        for i, (name, val) in enumerate(mutable_args):
            val = str_to_num(val)

            if not name:
                name = 'unknown'
            name = layer_name + ':' + name  # include layer name
            mutable_args[i] = MutableArg(val, name=name)

        # sub back in tuple args first so we don't have extra '{}'
        # then replace our '%s' token with '{}'
        arg_list_str = mutable_arg_list_str.format(*tuple_args).replace('%s', '{}')

        # add back on layer constructor
        layer_str = layer_str.format(arg_list_str)

        return mutable_args, layer_str

    def _extract_all_mutable_layers(self, build_str):
        """
        'extracts' all mutable layer sources from the raw source code
        :return: mutable_layer_sources, modified_build_str
        """

        # nothing to do if there are no layers to extract
        if not len(self.mutable_layer_names):
            return [], build_str

        tuple_pattern = r'(?:\(.*?\))'

        # the {} format params will be filled immediately just there to make things clean
        # the %s is there to insert the class name of the mutable keras layer
        mutable_layer_pattern_fmt_str = r'%s\((?:{}|.|\n)*?\)'.format(tuple_pattern)

        # make pattern that will match any class name
        mutable_layer_name_pat = ''

        for layer_name in self.mutable_layer_names:
            mutable_layer_name_pat += '(?:%s)|' % layer_name

        # remove last or (i.e. '|')
        mutable_layer_name_pat = '(?:%s)' % mutable_layer_name_pat[:-1]
        mutable_layer_pattern = re.compile(mutable_layer_pattern_fmt_str % mutable_layer_name_pat)

        mutable_layers, build_str = self._extract_pattern_occurrences(mutable_layer_pattern, build_str)

        return mutable_layers, build_str

    def _extract_mutable_args_from_settings(self):
        """ searches self.net_wrapper for 'mutable settings'
            to use in chromosome """
        mutable_settings = []

        for setting_name in self.mutable_settings_names:
            try:
                def_val = eval('self.net_wrapper.' + setting_name)
                assert is_basic_num(def_val)
                mutable_settings.append(MutableArg(def_val, 'Setting:' + setting_name))
            except AttributeError:
                pass

        return mutable_settings

    # verified to work experimentally (6/12/18)
    def _extract_mutable_args_from_source(self):
        """
        exracts full all mutable numeric args
        from raw build string obtained from network wrapper
        do build_src_fmt_str.format(*chromosome) to reconstruct
        build_src_str
        :return: mutable_args
        """

        banned_code, build_str = self._extract_banned_patterns(self.net_build_src)

        mutable_layers, build_str = self._extract_all_mutable_layers(build_str)
        mutable_args = []

        for i, layer in enumerate(mutable_layers):
            layer_args, layer = self._extract_mutable_args_from_layer(layer)

            # populate args and update layers so args are removed
            mutable_layers[i] = layer
            mutable_args.extend(layer_args)

        build_str = build_str.format(*mutable_layers)
        self._build_src_fmt_str = self._replace_banned_code(banned_code, build_str=build_str)

        return mutable_args