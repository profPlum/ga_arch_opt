#!/usr/bin/env python
"""
Genetic Algorithm ARCHitecture OPTimizer ---

This script uses a genetic algorithm to automate the task of
finding appropriate hyper parameters for our neural networks. The only
requirements for using this on a network is that the network trainer conforms to the
KnetWrapperInterface, and that you configure this script properly.

NOTE: many important but often unchanged config vals are defined as static variables
in the NetworkModifier class, read the docs there then save and modify a config to
your liking

TODO: spread across multiple files & create submodule
TODO: fix bug where optimizing training params creates nans
^ or don't...

TERMINOLOGY:
    * 'hyper-param' (sometimes ambiguously referred to as variable or arg) is a parameter to the
      network who's value isn't normally learned and thus is expected to be provided by the programmer
      however the whole point in this script is be able to 'learn' those too

    * 'gene' means one fragment or element of a full solution, in this case it would the
      value of a single mutable hyper-param

    * 'chromosome' or 'individual' means a full candidate solution or its 'DNA' which
      is made up of genes
"""


import argparse
import inspect
import random
import traceback
from operator import attrgetter
import functools
from typing import Callable

from deap import base, creator, tools, algorithms
from keras import backend as K

from ga_arch_opt.NetworkModifier import Chromosome, NetworkModifier
from dep.utils import *

import keras_network as knet

# redefine this as necessary,
# NOTE: not used inside GeneticArchOptimizer!
knet_factory = knet.GW_NetworkWrapper_factory

try:
    from mpi4py import MPI

    MPI_RANK = MPI.COMM_WORLD.Get_rank()

    ########### make seed unique ###########

    # seed is 32bit unsigned int,
    # but needs to be to power of 31
    # for some reason
    max_seed_val = 2 ** 31

    seed = random.randint(0, max_seed_val)
    seed = seed * (MPI_RANK + 1) % max_seed_val
    random.seed(seed);
    np.random.seed(seed + 4);
    del seed
    # for making network weight initialization random

    ########################################
except ImportError:
    MPI_RANK = 0
    warnings.warn('mpi4py import failed, without this module the code cannot run in parallel')


# just a namespace
class Loggers:
    ################### for dependency injection ###################
    # dummy dependency
    class _IndvSeqWrapper():
        pass

    class _HofSeqWrapper(_IndvSeqWrapper, tools.HallOfFame):
        pass

    class _ListSeqWrapper(_IndvSeqWrapper, list):
        def insert(self, indv):
            self.append(indv)

    class _IndvSeqLogger(_IndvSeqWrapper):
        def __init__(self, test_chrom, *args, **kwds):
            # we use this for mapping floats to real args
            self._test_chrom: Chromosome = test_chrom

            super().__init__(*args, **kwds)

        @property
        def raw_individuals(self):
            """ returns a generator of tuples like (raw_genes_generator, fitness) """
            for indv in super().__iter__():
                self._test_chrom.norm_floats = indv
                raw_indv = type(indv)(self._test_chrom.raw_genes)
                raw_indv.fitness = indv.fitness
                yield raw_indv

        # verified to work 7/21/18
        def load_raw_individuals(self, raw_indvs):
            """ for resuming & seeding """
            super().clear()

            for raw_indv in raw_indvs:
                self._test_chrom.raw_genes = raw_indv
                new_indv = type(raw_indv)(self._test_chrom.norm_floats)
                new_indv.fitness = raw_indv.fitness
                super().insert(new_indv)

        def log_to_csv(self, log_fn):

            # only worth keeping the most recent hall of fame
            with open(log_fn, 'w') as f:
                f.write('# individuals written in order of fitness\n')
                f.write('Fitness:weighted, Fitness:loss, Fitness:size, ')
                f.write(', '.join(self._test_chrom.unique_gene_names) + '\n')

                for indv in self.raw_individuals:
                    # write csv line to file
                    f.write('{}, {}, {}, '.format(*indv.fitness.values))
                    f.write(', '.join(map(str, indv)) + '\n')

    ################################################################

    # same as old hall of fame
    class HallOfFameLogger(_IndvSeqLogger, _HofSeqWrapper):
        pass

    # logging functionality of old HoF with a list backend
    # (to avoid potential unwanted side-effects)
    class PopLogger(_IndvSeqLogger, _ListSeqWrapper):
        pass


# TODO: fix bug where these flexible sessions can't be shared sometimes
def reset_dynamic_tf_sess(preallocate=False, gpu_mem_prealloc_fraction=0.24):
    """
    clears and recreates the global default tensorflow session (used by keras)
    by resetting this default session it manages a tensorflow memory leak.
    also the new global default session will use dynamic memory allocation so that
    training on a single node with multiple gpus can happen in parallel
    (by default tensorflow allocates all the memory of all available gpus which doesn't work for us)
    :param preallocate: if True new session will preallocate memory from each visible gpus
    :param gpu_mem_prealloc_fraction: if preallocate==True, the new tf session will allocate this fraction
                                      of each visible gpu's memory (pre-allocation like this is faster but less flexible)
    """

    import tensorflow as tf

    config = tf.ConfigProto()
    config.log_device_placement = False  # to log device placement (on which device the operation ran)
    # (nothing gets printed in Jupyter, only if you run it standalone)

    if preallocate:
        config.gpu_options.per_process_gpu_memory_fraction = gpu_mem_prealloc_fraction
    else:
        config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU

    K.clear_session()  # maybe not needed, but looks like it is
    sess = tf.Session(config=config)
    K.set_session(sess)  # set this TensorFlow session as the default session for Keras
    # NOTE: this set_session() call caused errors in ghpcc for no apparent reason


# TODO: decouple train_on_training_seq()
# NOTE: you will also need to reimplement train_evaluate()
# & knet wrapper loading for your class
class KnetWrapperInterface:
    @property
    def num_trainable_params(self) -> int:
        # use: k_get_num_trainable_params()
        raise NotImplementedError()

    @property
    def cfg_name(self) -> str:
        raise NotImplementedError()

    def build_clean_model(self):
        raise NotImplementedError()

    # NOTE: optional if changed in NetMod config
    def _build_clean_model(self):
        raise NotImplementedError()

    def train_on_training_seq(self, training_seq_name: str, no_plots: bool, TR_dir_prefix: str,
                              final_eval_fitness_eq: Callable[[float, float], float]):
        """
        :param final_eval_fitness_eq: equation to compute final evaluation fitness given the loss_fitness & size.
        e.g. lambda loss_fit, size: loss_fit
        :return: loss_fitness
        """
        raise NotImplementedError()

    def load_cfg(self, cfg_name: str):
        raise NotImplementedError()

    def save_cfg(self, cfg_path: str):
        raise NotImplementedError()


# specified for MPI workers
if 'OVERRIDING_TRAINING_SEQ_NAME' not in globals():
    OVERRIDING_TRAINING_SEQ_NAME = ''


# verified to work 7/9/18,
# NOTE: 100% static because MPI requires it
class GeneticArchOptimizer(object):
    """this encapsulates the logic of the actual genetic algorithm"""

    @staticmethod
    def _save_logbook_to_csv(logbook, path='logbook.csv'):
        """
        doing so is non-trivial, we got this from here:
        https://stackoverflow.com/questions/42904904/saving-deap-results-into-pandas-dataframe
        NOTE: this method doesn't work when there are more than 2 losses being tracked
        """
        import pandas as pd
        from functools import reduce
        from operator import add, itemgetter

        chapter_keys = logbook.chapters.keys()
        sub_chaper_keys = [c[0].keys() for c in logbook.chapters.values()]

        data = [list(map(itemgetter(*skey), chapter)) for skey, chapter
                in zip(sub_chaper_keys, logbook.chapters.values())]
        data = np.array([[*a, *b] for a, b in zip(*data)])

        columns = reduce(add, [["_".join([x, y]) for y in s]
                               for x, s in zip(chapter_keys, sub_chaper_keys)])
        df = pd.DataFrame(data, columns=columns)

        keys = logbook[0].keys()
        data = [[d[k] for d in logbook] for k in keys]
        for d, k in zip(data, keys):
            df[k] = d

        df.to_csv(path, index=False)

    @classmethod
    def _elite_selection_wrapper(cls, individuals, k, selector, fit_attr="fitness"):
        """ wrapper that implements elitism for any selection method """

        if cls.num_elites > 0:
            assert cls.deap_alg_name == 'eaMuPlusLambda'
            # elitism can't function properly with the default algorithm
            # (since elites can be destroyed with mutation & crossover)

        # reverse so fittest individuals first
        sorted_pop = sorted(individuals, key=attrgetter(fit_attr), reverse=True)
        elites = sorted_pop[:cls.num_elites]
        others = selector(sorted_pop[cls.num_elites:], k - cls.num_elites, fit_attr=fit_attr)

        return elites + others

    @classmethod
    def _create_deap_types(cls):
        # ignore warnings about overwriting old classes
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")

            # weights *aren't* used in w-sum, so we implemented weighted sum ourselves
            creator.create('FitnessMax', base.Fitness, weights=(1, 1, 1))
            creator.create('Individual', list, fitness=creator.FitnessMax)

    @classmethod
    def _get_toolbox(cls):
        """ creates DEAP types & toolbox that define the GA algorithm """

        try:
            if cls.__toolbox:
                return cls.__toolbox
        except AttributeError:
            pass  # if it doesn't exist it is essentially None

        cls._create_deap_types()

        toolbox = base.Toolbox()

        toolbox.register('evaluate', cls._train_evaluate)

        # per individual
        num_genes = len(cls._net_modifier.chromosome)

        # NOTE: uniform is also a valid distribution to sample from
        # toolbox.register('rand_gene', np.random.normal, 0.5, 0.2)
        toolbox.register('rand_gene', np.random.uniform, 0.0, 1.0)
        toolbox.register('rand_individual', tools.initRepeat, creator.Individual,
                         toolbox.rand_gene, n=num_genes)

        toolbox.register("mate", tools.cxOnePoint)
        if cls.double_search_op_strength:
            toolbox.register("mate", tools.cxTwoPoint)

        # indpb: chosen from standard definition (from wikipedia)
        # sigma: chosen so that float mutations will often keep the
        # resulting gene float between 0 and 1
        indpb=1.0 / num_genes if not cls.double_search_op_strength else 2.0 / num_genes
        toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=0.2, indpb=indpb)
        # NOTE: Gaussian distribution is defined on the reference interval
        # such that sigma in the "physical" (or raw) interval will have different sizes
        # NOTE: what we are drawing is the change, x, in the parameter value
        # such that param_new = param_old + x

        # IMPORTANT: can't use Roulette selection because it doesn't play nice with
        # multiple objectives or negative fitnesses
        toolbox.register("_select", tools.selTournament, tournsize=cls.tournament_size)
        toolbox.register("select", cls._elite_selection_wrapper, selector=toolbox._select)
        # wrap selector for elite selection

        cls.__toolbox = toolbox
        return toolbox

    @classmethod
    def _del_toolbox(cls):
        """ deletes the toolbox so that it will be regenerated """
        cls.__toolbox = None

    @classmethod
    def _init(cls):
        """ sets independent static variables to defaults,
        but does NOT initialize to a valid state for evolution! """

        cls.num_elites = 0  # num of best individuals that will move untouched into next generation
        cls.population_size = 25  # num of total individuals in each generation
        cls.num_generations = 20  # num of generations before termination
        cls.cxpb = 0.4  # probability that any given offspring will be produced by breeding
        cls.mutpb = 0.1  # probability that any given offspring will be produced by mutation
        # cxpb & mutpb inspired from: load forcasting GA paper
        # mutpb tripled though because monte-carlo appeared to be beating GA
        cls.double_search_op_strength=False

        cls.tournament_size = 2  # size of tournament used when selecting the fittest individuals

        # Weight of size shrunk after realization that even without optimizing size directly,
        # the best solutions were smaller. So we don't want to compromise accuracy too much.
        cls.loss_fit_weight = 0.975
        cls.size_fit_weight = 0.025

        cls._supported_alg_names = ['eaSimple', 'eaMuPlusLambda']
        cls.deap_alg_name = 'eaSimple'  # name of GA alg variant

        # NOTE: prob need to have preallocation off
        # if we have a single server node with many workers/gpus
        cls.preallocate_gpu_memory = False

        cls.quite = False
        cls.save_old_logs = True
        # if True will not delete any logs from previous runs or previous generations

        # whether the algorithm is parallelized & we are an MPI worker
        cls._distributed_worker = 'MPI_WORKER_GA_CFG_NAME' in globals()

        cls._GA_cfg_name = None

        # reset toolbox so a up-to-date one is created
        cls._del_toolbox()

    @classmethod
    def init(cls, knet_wrapper: KnetWrapperInterface, GA_cfg_name=None, NM_cfg_name=None):
        """
            a static version of a constructor
        :param knet_wrapper: knet_wrapper of network to evolve
        :param GA_cfg_name: name of GA config to load  (optional)
        :param NM_cfg_name: name of config for NetworkModifier (optional)
        """

        # reset/init independent state
        cls._init()

        if NM_cfg_name:
            NetworkModifier.load_cfg(NM_cfg_name)

        if GA_cfg_name:
            cls.load_cfg(GA_cfg_name)

        cls._knet_wrapper = knet_wrapper
        cls._net_modifier = NetworkModifier(cls._knet_wrapper)
        cls._default_trainable_params = cls._knet_wrapper.num_trainable_params

        cls._create_deap_types()

    @classmethod
    def _train_evaluate(cls, gene_list):
        """
        Evaluates the fitness of the hyper-params of a Neural Network
        in the given chromosome.

        NOTE: Fitness scores are both normalized
        although not necessarily with the same
        distribution.
        :param gene_list: list of genes (i.e. the chromosome) in norm float form
        :return: loss_fitness, size_fitness
        """

        try:
            # avoids tensorflow memory leak
            reset_dynamic_tf_sess(preallocate=cls.preallocate_gpu_memory)

            # assign new test chromosome
            cls._net_modifier.chromosome.norm_floats = gene_list
            cls._net_modifier.apply_chromosome()

            if not cls.quite:
                print("\ntesting new chromosome:\n")
                print("#" * 50)
                print('gene names:', list(cls._net_modifier.default_chromosome.gene_names))
                print('default chromosome: ', list(cls._net_modifier.default_chromosome.raw_genes))
                print('test chromosome: ', list(cls._net_modifier.chromosome.raw_genes))

            TR_dir_prefix = '.'
            if cls._distributed_worker:
                TR_dir_prefix = 'TR_MPI_RANK' + str(MPI_RANK)

            cls._knet_wrapper.build_clean_model()

            size_fitness_eq = lambda size: (cls._default_trainable_params - size) / float(cls._default_trainable_params)
            w_fitness_eq = lambda loss_fit, size: loss_fit*cls.loss_fit_weight + size_fitness_eq(size)*cls.size_fit_weight
            # code discovered experimentally using terminal 5/5/19

            global OVERRIDING_TRAINING_SEQ_NAME  # this isn't necessarily set, if so it doesn't override
            fitness = cls._knet_wrapper.train_on_training_seq(training_seq_name=OVERRIDING_TRAINING_SEQ_NAME,
                                                              no_plots=True, TR_dir_prefix=TR_dir_prefix,
                                                              final_eval_fitness_eq=w_fitness_eq)

            size_fitness = size_fitness_eq(cls._knet_wrapper.num_trainable_params)
            return w_fitness_eq(fitness, cls._knet_wrapper.num_trainable_params), fitness, size_fitness

        except:
            err_str = traceback.format_exc()
            print("Exception in MPI worker with rank={}:\n{}\n".format(MPI_RANK, err_str),
                  file=sys.stderr, flush=True)
            raise

    @classmethod
    def _make_seeded_pop(cls, seeds: list, seed_portion: float, n: int = None, copy_elites=False):
        """
            makes a seeded population using completely random individuals,
            mate() and a random seed, and one exact copy of the first cls.num_elites
            seeds (seeds are expected to be in order of fitness)

            NOTE: expects that cls._toolbox has been made by calling cls._make_toolbox()
            :param seeds: list of high quality individuals to
                          seed the population, in order of fitness
            :param seed_portion: portion of the initial population that
                                 will be 'seeded' with verbatim copies
                                 or mate(), if 0 pop is random...
            :param n: pop size, None uses cls.population_size
            :param copy_elites: whether to copy the elites from the last run (based on num_elites setting),
                                by default this is turned off because it could be unfair to a new pop
            :return: pop
        """

        if n is None:
            n = cls.population_size

        # assert these are individuals
        for seed in seeds:
            assert isinstance(seed, creator.Individual)

        # total seeded in some fashion (copied or bred)
        n_seeded = int(seed_portion * n + 0.5)

        # verbatim copies are effectively elites (from last run)
        n_copied = max(cls.num_elites, n_seeded) if copy_elites else 0
        n_bred = max(n_seeded - n_copied, 0)
        n_rand = n - n_bred - n_copied

        # add verbatim copies first
        pop = list(seeds)[:n_copied]

        # can't use toolbox.select() because tournament
        # size is chosen for a larger population
        chosen_seeds = tools.selTournament(seeds, k=n_bred, tournsize=2)

        toolbox = cls._get_toolbox()

        # make seeded individuals
        for seed in chosen_seeds:
            pop.append(toolbox.mate(seed, toolbox.rand_individual())[0])
            del pop[-1].fitness.values

        # next make purely random individuals
        pop += [toolbox.rand_individual() for _ in range(n_rand)]

        return pop

    @classmethod
    def _make_stats_recorder(cls):
        """tells GA whan run-time stats to record"""
        get_fit = lambda ind, i: ind.fitness.values[i] if ind.fitness.valid else 0.0

        # NOTE: save_logbook_to_csv doesn't work when tracking more than 2 losses...
        # stats_weighted_fit = tools.Statistics(key=lambda ind: get_fit(ind, 0))
        stats_loss_fit = tools.Statistics(key=lambda ind: get_fit(ind, 1))
        stats_size_fit = tools.Statistics(key=lambda ind: get_fit(ind, 2))
        mstats = tools.MultiStatistics(#weighted_fit=stats_weighted_fit,
                                       loss_fit=stats_loss_fit, size_fit=stats_size_fit)

        # makes a statistic function that casts the numpy output to
        # a regular python scalar that we can read when this is saved
        # to a yaml file
        make_stat_readable = lambda np_func, x: np_.cast_to_py_scalar(np_func(x))

        mstats.register("avg", make_stat_readable, np.mean)
        mstats.register("mad", make_stat_readable, np_.mad)
        mstats.register("min", make_stat_readable, np.min)
        mstats.register("max", make_stat_readable, np.max)
        return mstats

    @classmethod
    def _make_new_GA_logs_dir(cls):
        if cls.save_old_logs:
            make_into_backup('GA_logs')
        else:
            shutil.rmtree('GA_logs')
        os.mkdir('GA_logs')

        # save our configs to GA_logs for reference
        GA_cfg_name = os.path.basename(cls._GA_cfg_name) if cls._GA_cfg_name else 'auto_gen_GA_cfg.yaml'
        knet_cfg_name = os.path.basename(cls._knet_wrapper.cfg_name) if cls._knet_wrapper.cfg_name else 'auto_gen_knet_cfg.yaml'
        NM_cfg_name = os.path.basename(
            NetworkModifier.cfg_name) if NetworkModifier.cfg_name else 'auto_gen_net_mod_cfg.yaml'

        cls.save_cfg('GA_logs/' + GA_cfg_name)
        cls._knet_wrapper.save_cfg('GA_logs/' + knet_cfg_name)
        cls._net_modifier.save_cfg('GA_logs/' + NM_cfg_name)

    @classmethod
    def _GA_loop(cls, population, start_gen, end_gen, hall_of_fame, logbook):
        """ GA loop that saves checkpoints & log info between generations """

        mstats = cls._make_stats_recorder()
        # the first time we will give it stats to record so we can get gen 0,
        # afterwards we need to do it manually, otherwise stats won't be recorded
        if len(logbook) == 0:
            stats_arg = mstats
        else:
            stats_arg = None

        # mu+lambda strategy:
        # mu: is the num to select for any given generation
        # lambda: is the num offspring to produce for any given generation
        # NOTE: if mutpd + cxpb < 1.0 (which it usually is) then some offspring are actually the parents

        # bind algs so they have common signature
        supported_algs = {'eaMuPlusLambda': functools.partial(algorithms.eaMuPlusLambda, mu=cls.population_size,
                                                              lambda_=cls.population_size),
                          'eaSimple': algorithms.eaSimple}
        assert all((alg_name in supported_algs for alg_name in cls._supported_alg_names))
        deap_alg = supported_algs[cls.deap_alg_name]

        pop_logger = Loggers.PopLogger(cls._net_modifier.chromosome)

        for gen_id in range(start_gen, end_gen):
            population, logbook_new = deap_alg(population, cls._get_toolbox(), stats=stats_arg,
                                               cxpb=cls.cxpb, mutpb=cls.mutpb, verbose=cls.quite,
                                               ngen=1, halloffame=hall_of_fame)

            # clip normalized floats to valid boundaries,
            # this stops them from gathering 'dead inertia'
            # (assignment to chromosome does this automatically)
            for indv in population:
                cls._net_modifier.chromosome.norm_floats = indv
                indv[:] = cls._net_modifier.chromosome.norm_floats

            if cls.save_old_logs:
                make_into_backup('./GA_logs/GA_best.csv')
                make_into_backup('./GA_logs/GA_pop.csv')
            hall_of_fame.log_to_csv('./GA_logs/GA_best.csv')
            pop_logger.clear()
            pop_logger.extend(population)
            pop_logger.log_to_csv('./GA_logs/GA_pop.csv')

            # logbook_new always has an entry for "generation 0"
            # but we only need one of these, everyone after the
            # first is incorrect & redundant
            if len(logbook) > 1:
                # record our stats manually,
                # the logbook_new still has 'num_evals' arg which we want
                logbook.record(gen=gen_id, nevals=logbook_new[1]['nevals'],
                               **mstats.compile(population))
            else:
                logbook = logbook_new  # get our gen 0 & gen 1
                stats_arg = None  # now we get stats manually

            # csv logbook is important
            cls._save_logbook_to_csv(logbook, 'GA_logs/current_logbook.csv')

            # keep latest in open for resuming & seeding
            with open('GA_logs/GA_checkpoint.yaml', 'w') as f:
                # we store only raw individuals and their fitness because
                # it gets complicated if we want to start storing the compare_f(unction)
                cp = {'pop': population, 'logbook': logbook, 'HoF': list(hall_of_fame.raw_individuals),
                      'netmod.chromosome': cls._net_modifier.chromosome}
                yaml.dump(cp, f)
        return logbook

    @classmethod
    def _start_evolution(cls, seed_portion=0.0, resume=False, checkpoint_path='GA_logs/GA_checkpoint.yaml'):
        """starts serial GA, use wrapper not this"""

        hall_of_fame = Loggers.HallOfFameLogger(cls._net_modifier.chromosome, maxsize=10)

        pop_seeds = []

        if seed_portion > 0.0:
            print('the best individual from the last run will make'
                  ' the starting architecture this time')
            print('seed portion = {}'.format(seed_portion))
            print('seeding with best from "{}"...\n'.format(checkpoint_path))

            try:
                # load checkpoint
                with open(checkpoint_path, 'r') as f:
                    checkpoint_dict = yaml.load(f)

                # have the first (best) seed define the default hyper params
                # of the network, doing this makes MutableArg mapped ranges flexible
                cls._net_modifier.chromosome.recompute_mutable_arg_mapped_ranges(
                    checkpoint_dict['HoF'][0], checkpoint_dict['HoF'])
                # hall_of_fame automatically sees the new mapped ranges

                hall_of_fame.load_raw_individuals(checkpoint_dict['HoF'])
                # if we want to seed we use the previous hall of fame

                pop_seeds = list(hall_of_fame)
            except FileNotFoundError:
                answer = ask_yes_no_question('GA_checkpoint.yaml file not found for seeding...\n'
                                             'Would you like to continue without seeding?')
                # if no then raise
                # the error as normal
                if not answer: raise

        # compare function stops HoF from filling up with the same individual (since it maps floats to ints).
        # made here so that HoF has access to the potentially recomputed mapped ranges
        hall_of_fame.similar = cls._net_modifier.chromosome.make_compare_func()

        if resume:
            print('resuming previous GA run...')

            # load checkpoint
            with open(checkpoint_path, 'r') as f:
                checkpoint_dict = yaml.load(f)

            # cls._net_modifier.chromosome.recompute_mutable_arg_mapped_ranges(
            # checkpoint_dict['HoF'][0], checkpoint_dict['HoF']+checkpoint_dict['pop'])
            cls._net_modifier.chromosome.load_from(checkpoint_dict['netmod.chromosome'])

            population = checkpoint_dict['pop']
            assert len(population) == cls.population_size
            hall_of_fame.load_raw_individuals(checkpoint_dict['HoF'])
            logbook = checkpoint_dict['logbook']

            start_gen = len(logbook)
            end_gen = start_gen + cls.num_generations

            # NOTE: if we are resuming then we use the old log dir
        else:
            population = cls._make_seeded_pop(pop_seeds, seed_portion)

            logbook = []  # dummy value
            start_gen = 1
            end_gen = cls.num_generations + 1

            # otherwise we make a fresh one
            cls._make_new_GA_logs_dir()

        print('save_old_logs={}'.format(cls.save_old_logs))
        print('starting evolution...')
        print()

        logbook = cls._GA_loop(population, start_gen, end_gen, hall_of_fame, logbook)

        print('done')
        return hall_of_fame, logbook

    @classmethod
    def start_evolution(cls, seed_portion=0.0, training_seq_name='', resume=False, distributed=False):
        """
            runs the genetic algorithm
            NOTE: currently using multiple nvidia GPUs per machine across multiple
                  machines isn't supported
        :param seed_portion: portion of the initial population that will be 'seeded'
                             with the hall of fame from previous run, also as long as
                             this is greater than 0 mutable arg mapped ranges will be
                             computed using the *best* individual from the hall of fame
        :param training_seq_name: this can override the training sequence to be trained on,
                                  since this can change often it can be manually specified here
        :param resume: whether to resume the previous GA run
        :param distributed: whether the GA will be run in parallel
        :return: hall of fame, population, logbook
        """

        # must be stored in a *global* (not even static will do)
        global OVERRIDING_TRAINING_SEQ_NAME
        OVERRIDING_TRAINING_SEQ_NAME = training_seq_name

        cls._del_toolbox()  # reset toolbox so a up-to-date one is created

        if distributed:
            worker_globals = {'MPI_WORKER_GA_CFG_NAME': cls._GA_cfg_name,
                              'MPI_WORKER_KNET_CFG_NAME': cls._knet_wrapper.cfg_name,
                              'MPI_WORKER_NET_MOD_CFG_NAME': NetworkModifier.cfg_name,
                              'OVERRIDING_TRAINING_SEQ_NAME': training_seq_name}

            # this process won't be using GPUs so we need
            # to deallocate memory resources (a dynamic session won't hog any)
            reset_dynamic_tf_sess()

            print('GA is currently distributed')

            from mpi4py.futures import MPIPoolExecutor
            with MPIPoolExecutor(globals=worker_globals) as executor:
                cls._get_toolbox().register("map", executor.map)
                return cls._start_evolution(seed_portion, resume)
        else:
            print('GA is currently not distributed')
            cls._get_toolbox().register("map", map)  # technically redundant...
            return cls._start_evolution(seed_portion, resume)

    @classmethod
    def save_cfg(cls, config_path):
        cfg_dict = {}

        for name, val in cls.__dict__.items():
            if name[0] != '_' and not inspect.isroutine(val):
                cfg_dict[name] = val

        with open(config_path, 'w') as f:
            yaml.dump(cfg_dict, f)

    @classmethod
    def load_cfg(cls, GA_cfg_name):
        """
        load a GA config, checks local dir & configs dir
        NOTE: this can be called by init(), which needs to be called anyways...
        :param GA_cfg_name: name of GA config
        """

        # reset state
        cls._init()

        cls._GA_cfg_name = GA_cfg_name
        with open(get_config_path(GA_cfg_name), 'r') as f:
            cfg_dict = yaml.load(f)
        for name, val in cfg_dict.items():
            print('setting cls.{} = {}'.format(name, val))
            exec('cls.{} = val'.format(name))

        assert cls.deap_alg_name in cls._supported_alg_names

    # should be in fitness tester...
    @classmethod
    def get_build_src_fmt_str(cls):
        return cls._net_modifier.build_src_fmt_str


GeneticArchOptimizer._init()  # initialize static independent variables

# hack of GA to convert it into monte carlo...
# ignore pop, just compare HoF's
#
# when cls.mutpb = 1.0 all of the population will be mutating
# when cls.indpb = 1.0 all of the genes will be mutating
# ... and in this setting its ordinary Monte Carlo
class MonteCarloOptimizer(GeneticArchOptimizer):

    @classmethod
    def _set_GA_args_for_MC(cls):
        cls.mutpb = 1.0
        cls.indpb = 1.0
        cls.cxpb = 0.0

    @classmethod
    def init(cls, *args, **kwargs):
        super().init(*args, **kwargs)
        cls._set_GA_args_for_MC()

    @classmethod
    def _get_toolbox(cls):
        toolbox = super()._get_toolbox()
        toolbox.register("mutate", tools.mutUniformInt, low=0, up=1, indpb=1.0)
        return toolbox

    @classmethod
    def load_cfg(cls, *args, **kwd_args):
        super().load_cfg(*args, **kwd_args)
        cls._set_GA_args_for_MC()


GA = GeneticArchOptimizer
NetMod = NetworkModifier

GA.save_cfg(GW_DNN_INSTALL_PATH + '/configs/default_GA_cfg.yaml')
NetMod.save_cfg(GW_DNN_INSTALL_PATH + '/configs/default_net_mod_cfg.yaml')
# save default configs for easy modification and loading

try:
    # load our config(s)
    knet_wrapper = knet_factory(MPI_WORKER_KNET_CFG_NAME)
    GA.init(knet_wrapper=knet_wrapper, GA_cfg_name=MPI_WORKER_GA_CFG_NAME,
            NM_cfg_name=MPI_WORKER_NET_MOD_CFG_NAME)
    print("MPI worker loading knet config:", MPI_WORKER_KNET_CFG_NAME)
    print("MPI worker loading GA config:", MPI_WORKER_GA_CFG_NAME)
    print("MPI worker loading NM config:", MPI_WORKER_NET_MOD_CFG_NAME)
except (TypeError, NameError):  # TypeError if MPI_WORKER_GA_CFG_NAME is None (i.e. no config)
    pass  # NameError if we aren't a worker

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Uses a genetic algorithm to evolve an optimal\n'
                                                 'architecture/configuration for our neural networks.')
    parser.add_argument('--resume', '-r', dest='resume', action='store_true',
                        help='(-r shorthand) have GA resume last run saved in GA_logs (note: undefined behaviour if'
                             ' you use configs different from last time)')
    parser.add_argument('--seed', '-s', dest='seed_with_best', action='store_true',
                        help='(-s shorthand) Tell GA to seed the starting population with\n'
                             'the best individuals from the last run (found in ./GA_best.csv).\n'
                             'Setting this will also cause the *best* individual from last run to\n'
                             'set the starting hyper-params for this run which in turn changes\n'
                             'their mapped ranges (i.e. allows them to take on new values).')
    parser.add_argument('--seed-portion', '-sp', dest='seed_portion', type=float, default=0.3,
                        help='(-sp shorthand) Specify portion of the GA starting population that\n'
                             'will be seeded with the best from the last run. Seeding only happens\n'
                             'if --seed (-s) flag is also provided.')
    # NOTE: never implemented prob not needed
    # parser.add_argument('--save-training-logs', '-sl', action='store_true',
    #                     help='by default the GA makes the network wrapper not save training logs or intermediate models,'
    #                          'with this option you can turn that off so that they are saved for later examination')
    parser.add_argument('--distributed', '-d', dest='distributed', action='store_true',
                        help='(-d shorthand) Have GA use MPI to run distributed across a cluster (or one server)')
    parser.add_argument('--training_sequence', '-t', dest='training_seq_name', default='',
                        help='(-t shorthand) specify the *name* of the training sequence to be used')
    parser.add_argument('-mc', '--monte-carlo', action='store_true',
                        help='Activate monte carlo mode, by overriding necessary settings/code in GA.'
                             'Only look at HoF for results, & set n with n=population_size*generations.')
    parser.add_argument('knet_cfg_name', type=str,
                        help='Name of knet config file that will be used to make the starting network')
    parser.add_argument('GA_cfg_name', nargs='?', type=str, help='Name of a config file for this script')
    parser.add_argument('NM_cfg_name', nargs='?', default=None, type=str,
                        help='Name of the network modifier config, this affects how our network\n'
                             'can be modified by the GA')
    args = parser.parse_args()

    if args.monte_carlo:
        GA = MonteCarloOptimizer

    GA.init(knet_wrapper=knet_factory(args.knet_cfg_name),
            GA_cfg_name=args.GA_cfg_name, NM_cfg_name=args.NM_cfg_name)

    if not args.seed_with_best:
        args.seed_portion = 0.0  # this will turn off seeding

    HoF, logbook = GA.start_evolution(seed_portion=args.seed_portion, training_seq_name=args.training_seq_name,
                                      resume=args.resume, distributed=args.distributed)

    for i, individual in enumerate(HoF.raw_individuals):
        print("\ncandidate #%d:" % i)
        print("#" * 50)
        print(GA.get_build_src_fmt_str().format(*individual))
        print("#" * 50)

    print("\nlogbook:")
    print(logbook)

    # save logbook in readable form
    with open('GA_logs/logbook_pretty.txt', 'w') as f:
        f.write(str(logbook))
